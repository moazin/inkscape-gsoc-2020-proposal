# Path Library Improvement Project

Google Summer of Code 2020 proposal to Inkscape. To skip to the meaty stuff, click [here](#path-library-improvement).

## About Me

Hi, I am a Senior Engineering student at Pakistan Institute of Engineering and Applied Sciences, Islamabad. Though I am being trained in Electrical Engineering (with emphasis on Embedded Systems) at my university, I have been into computer programming since 7th grade and I plan to build a career as a Software Developer. I have explored various domains of Software Development such as mobile, desktop, web (full-stack) and scientific computation. I have also participated in Google Summer of Code 2019 with the FreeType project where I added OpenType SVG font support.

1. **What is your name, email address, and IRC nickname?**
    - **Legal Name**: Moazin Khatti
    - **Email**: moazinkhatri@gmail.com
    - **Gitlab, Github, IRC, RocketChat**: @moazin  
2. **Describe any plans you have for the summer in addition to GSoC (classes, thesis, job, vacation, etc)**  
I was expecting to graduate in June 2020, so there would have been commitments to university work for about 2 weeks. However, due to COVID-19, my university is off and going to remain off for a long time. There hasn't been any official notification about when it'll re-open and we are still waiting to hear from them. I understand that GSoC is supposed to be treated like a full-time job. Right before finalizing the applicants  Google sends the candidates an email asking to withdraw if the situation has changed for them. I'll make sure to withdraw if there is any chance of me being busy in summer. I don't want to fail and I don't want Inkscape to lose a slot either.


3. **What programming projects have you completed?**  
I have worked in many areas such as building desktop applications, single page web applications, C/C++ based libraries, mobile applications and embedded systems. Here are a few recent ones:

    * [Adding OpenType-SVG font support to FreeType](https://summerofcode.withgoogle.com/archive/2019/projects/6002250785226752/) (GSoC 2019).
    * Easylend, a money sharing application. ([mobile app](https://github.com/moazin/easylend-android-app) and [server](https://github.com/moazin/easylend-server))
    * Building a vehicle tracking system. Both the devices and the server as part of my thesis project.
    * Developed and deployed an event registration system that managed the participation information (fee, modules and teams) of over 1000 participants for a university event. (Developed with Django, Django Rest Framework, ReactJS and deployed with Nginx + Gunicorn).
    * Developed and deployed a clinical laboratory report generation system in Django.

4. **What are your favorite programming tools (editor, etc.)?**
    * *Editor*: Vim + Tmux = ❤️
    * *Languages*: C, C++, Python 3, Javascript, Java
    * *OS*: Ubuntu
  
5. **Describe any work on other open-source projects**  
My GSoC 2019 project with FreeType linked above and a few PRs in Adobe's [SVG Native Viewer Lib](https://github.com/adobe/svg-native-viewer).

6. **List other GSoC projects you are applying to**  
Applying to **FreeType** for *Reimplementing font-rs in FreeType*.

## Inkscape's Questions

1. **When did you first hear about Inkscape?**  
Sometime around 2016 when I was designing posters for university events on Adobe Illustrator, I was looking for open source alternatives. That's when I found Inkscape.
2. **What kind of drawings do you create with Inkscape?**  
Mostly technical block diagrams that I attach in course reports, presentations and documents. Sometimes simple illustrations too. Few shown below.

3. **Describe your participation in our community?**  
Been hanging around in the chat for the past month mostly exploring things related to this project.
4. **Describe your contributions to the  Inkscape development.**
    * [Fixes inbox#1942 - Select all elements with specific filter](https://gitlab.com/inkscape/inkscape/-/merge_requests/1527) (Merged)
    * [WIP: Fix #347 - push tree selection when "type" is changed](https://gitlab.com/inkscape/inkscape/-/merge_requests/1525)
    * [WIP: Fix #964 by simplifying paths after inset/outset](https://gitlab.com/inkscape/inkscape/-/merge_requests/1601)
    * [Fix incorrect vim modelines](https://gitlab.com/inkscape/lib2geom/-/merge_requests/25)
5. **In exactly two sentences, why should we pick you?**
    * I LOVE exploring and understanding complex systems. (Which is a big part of this project)
    * I have the experience and the capability to complete this project under minimal guidance from the mentors. 

![inkscape drawings](./gsoc-pics.jpg)

## Path Library Improvement

Inkscape relies on two libraries for its geometry related operations. One is *lib2geom* which was designed specially for Inkscape and the other is *livarot*, a library written in 2003 that has some unique features which *lib2geom* doesn't have. Features such as:

* Path Simplification
* Path Flattening
* Line Scanning (flowing text)
* Path Offset/Inset
* Boolean Operations
* Some modes of Tweak Tool

While *livarot* is functionally good, it's quite outdated. It was written in 2003, it's extremely hard to understand it or maintain it. The goal of this project is to implement all of these features either in *lib2geom* or outside it while using *lib2geom's* features as much as possible. 

To understand about how Livarot works visit this [wiki](https://wiki.inkscape.org/wiki/index.php?title=Exploring_Livarot).

#### Proposed Timeline

1. **Community Bonding Period and Before**
    * Experimenting with PaperJS and reading Krzysztof Kosiński's work on *boolean operations* that lives in `Geom::IntersectionGraph` to see if it makes sense to change the approach altogether. (For now it doesn't)
    * Study *livarot*. I **must** know how all the functions being used by Inkscape work. I have already studied some important ones but there is a lot more to do.
    * Study *livarot's* newer version and *lib2geom* to see how the features of *livarot* can be redesigned while using *lib2geom's* existing functionalities and moden C++ features as much as possible. *lib2geom* already has an intersection finding algorithm that can find intersections between two `Geom::PathVector` objects. *lib2geom* also has a `Sweeper` that will be very useful.
    * Research on efficient algorithms to approximate common forms of curves (Quadratic Beziers, Cubic Beziers and Arcs) with line segments.
        * Raph Levien's [Cubic to Linear converter](https://levien.com/tmp/flatten.html) is one good candidate.
    * Study how the offset code in *src/object/sp-object.cpp* manages to find the right threshold for simplification such that any detail is not removed while redundant nodes are also minimized. See if there are even better ways. This should solve many bugs which appear when doing *boolean operations* or *path outset/inset*.

2. **Week 1-2**
    * Create `Polyline` class that can hold an approximated version (by line segments) of a `Geom::PathVector`.
    * Write code that can convert a `Geom::PathVector` to a `Polyline` given a threshold value. 
    * Write code that can convert a `Polyline` back to a `Geom::PathVector` (essentially simplifying it) by fitting lines and curves. (This will also take a threshold value)
    * Replace the code in *pencil tool*, *simplify* and *lpe-simplify* to use this class and test.

3. **Week 3-5**
    * Implement a `Polygon` class that will store a directed graph with focus on using *C++ Standard Library*, *boost* and *lib2geom* as much as possible.
    * Write code that can create a `Polygon` from a `Polyline`.
    * Write code that can remove self-intersecting regions (basically the equivalent of `Shape::ConvertToShape`). A huge part of `Shape::ConvertToShape` is just a *sweepline* algorithm which already exists in *lib2geom*.
    * Write code that can extract contours from the `Polygon` back to a `Polyline` which can be simplified and converted to a `Geom::PathVector`.
    * Replacing the code in *lpe-offset* to use the new `Polygon` class for removing self-intersections.

4. **Week 6-9**
    * Implement *boolean operations*.
    * Implement *shape tweaking*.
    * Implement *line scanning*.
    * Replace all the code which used to rely on *livarot* for these features and test.

5. **Week 10-12**
    * Buffer time to catch up if there are things left.
    * Test and write documentation.
    * A stretch goal would be to study how the newly implemented stuff can be placed in *lib2geom*.
