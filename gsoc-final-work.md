# Path Library Improvement Project
| Field        | Value                                                          |
|--------------|----------------------------------------------------------------|
| Project Name | Path Library Improvement                                       |
| Organization | Inkscape                                                       |
| Project URL  | https://summerofcode.withgoogle.com/projects/#6513598572003328 |
| Mentors      | Thomas Holder, Marc Jeanmougin, Krzysztof Kosinski             |
| Student Name | Moazin Khatti                                                  |
| Email        | moazinkhatri@gmail.com                                         |

## Background

Inkscape is a popular vector graphics editor. It's lightweight and has excellent features. Since Inkscape is a vector graphics software, there is always some computational/mathematical work being done whenever you're doing some operation. For example, when you rotate something, there is a rotation transformation matrix that gets multiplied to each point of your shape. Inkscape relies on a library named lib2geom for most of its computational geometry needs. There is however a very special set of features that lib2geom doesn't have. For that, Inkscape relies on a library called livarot. It was written and integrated in Inkscape somewhere in 2003 when work on Inkscape started. It received some updates near that time but since then it has remained unchanged in Inkscape. People who understood it and worked on it left the community. Inkscape ended up with a very important piece of code that's too cryptic to understand and maintain. We need Livarot for the following features:
1. Path Simplification
2. Boolean Operations
3. Path Offsetting
4. Flowing Text (Line Scanning)
5. Tweak Tool (some modes of it)

![](./livarot-features.png)

The goal of this project was to firstly understand the complex and undocumented parts of Inkscape and then to create a good redesign that doesn't repeat the same problems that Livarot has.

## Work Product

I have written extensive documentation that explains everything I've learned within the project coding period and I've created a simpler redesign that can do path simplification as well as path flattening (removing self intersections).
1. All the documentation I wrote is available from [document-livarot-doxygen](https://gitlab.com/moazin/inkscape/-/tree/document-livarot-doxygen). \[ last commit within GSoC 9c8bdd1aa8842958e3ed57582d8a019d84fd3e7f\]
2. My redesign code is available from [attempt-convert-shape](https://gitlab.com/moazin/inkscape/-/tree/attempt-convert-shape/). \[ last commit within GSoC cab168dd4d51263a82bab6b6d9b67b6469b6fa31\]

## What was done and what's left to do

I firstly documented everything that I've learned. My documentation consists of an overview page, header file documentation for each useful function of Livarot and comments in code wherever needed. So now a very important part of Livarot that no one understood, is now well understood and its important parts are well documented. This should allow anyone to modify the code or redesign it. During my project, I worked on a redesign too. It's able to do Path Simplification and Path Flattening (removing self-intersections). The latter is an extremely fundamental part of Livarot that's used by almost every other operation. My redesign is quie similar to Livarot except that I've used better naming, simpler data structures and cleaner logic. However, I recently discovered that an important part of Livarot that I considered useless isn't really useless and this renders my design incomplete.

Some parts of Livarot such as Tweaking, Offsetting and Scanning need to be understood and documented. I expect this to be far easier and simpler since firstly, I've already documented the hardest and fundamental part and secondly, the code behind these is very small in size. After this, a ground up redesign of Livarot is needed. Once we know how everything works, we can start from scratch and do a total redesign that has cleaner code.

## Chronological summary of my work

I knew when I applied for the project that it was going to be hard, frustrating and would take a lot of time to complete. However, back then, I didn't have a clear idea of how hard it would be to understand Livarot. I planned such that I'll read and understand existing code within Community Bonding Period and do the redesign within Coding Period. I got sick within the Community Bonding Period and couldn't do it. When the coding period started, I started working on Path Simplification feature. I understood how the existing code worked and finished a redesign within two weeks.

I had to work on Path Flattening next. Path Flattening is fundamental to Livarot since its used by almost all features except Path Simplification. It's extremely cryptic and hard to understand. The process of Path Flattening consists of few steps:

1. Creating a directed graph.
2. Finding all intersections.
3. Rounding edges, reconstructing such that intersections are all vertices now.
4. Doing a depth first search on edges to calculate winding numbers.
5. Manipulating edges depending on the fill rule and winding numbers.
6. Doing a depth first search to extract contours.

1, 4, 5, 6 were all easy to understand. 2 was hard but with help from books on Computational Geometry I was able to understand it too. 3 however, was tougher than anything else. It is the most complicated part of Livrot that I've seen. I spent many days trying to understand and make sense of it all but couldn't do so. I could read code, but I couldn't understand why it was doing something simple in such a complicated way and why there is so much code that looks useless. Recreating this code as it is would be totally useless. A good redesign wasn't an option since there was still a lot I needed to learn. So I decided to just recreate something that's identical to Livarot in design but uses simpler data structures, has better naming and that I'd only include parts that I understand, leaving out everything that seemed useless. I replaced the data structures, used a double linked list instead of an AVL tree and a priority queue from STL. I used sorting algorithms from STL. I started testing my code and Livarot on identical shapes. I'd analyze logs from both to see where they differed and fix those parts. Doing this countless times, my code started producing identical results on all paths I drew by mouse. Identical results from both also indicated that I was right about Livarot having useless code, since my version doesn't have it and seems to work just fine. I was happy because my code was simpler and worked great too. At the same time, I was really skeptical about how Livarot could have code that did nothing. Maybe it was planned to have some use but the author never wrote those parts, so I thought. This was the progress in first two months. I had a meeting with my mentors where they asked me to make sure that I document everything I have learned about Livarot. This is so that no matter what happens to the project, at least this knowledge would remain preserved forever allowing anyone else to later take up the project and complete it.

So I started documenting everything. I wrote header file doxygen formatted documenation for all useful and important functions of Livarot. These describe what the function does and how it does it. They have SVG figures embedded wherever needed to support the explaination. Functions have inline comments wherever needed to make things clear. I also wrote an overview page that explains the theory behind the whole process.

While I was writing the documentation for a function whose job is to find intersections and thinking about how certain variables look like from different points in the code, I suddenly had an epiphany. I realized that there are two purposes of all the chaos that I previously thought was useless. Firstly, they deal with adjacencies (intersections where the intersection point is the endpoint of one of the segments) and secondly, it's responsible for rounding. Livarot does something called Snap Rounding using an algorithm based on a paper titled ["Practical segment intersection with finite precision output"](https://www.sciencedirect.com/science/article/pii/S0925772199000218) by John D. Hobby. Livarot doesn't clearly mention this apart from a reference to "hooby's algorithm" but I think the author was referring to the one described in this paper. So the conclusion is that all of the chaos in that region of code was useful, but it gets triggered in rare cases. Those parts become useful when points and edges get too close to each other. Nevertheless, it's a part that is needed.

I documented these findings too as clearly as I could.
