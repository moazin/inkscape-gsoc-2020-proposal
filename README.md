# GSoC 2020 Proposal to Inkscape

The file [proposal.md](./proposal.md) contains my proposal. To directly jump to the meaty stuff click [here](./proposal.md#path-library-improvement). To provide feedback you can create issues or contact me personally.
